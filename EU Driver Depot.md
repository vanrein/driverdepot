# European Driver/Manual Depot

> *Devices sold to Europeans must not stop being useful
> by mere choice of a manufacturer.  Manuals must
> continue to be downloadable, and software must be
> available for further development.*

To this end, a European Driver Depot and a European Manual Depot are available to manufacturers.  These depots contain driver manuals and a complete software stack for building and updating any related software, such as drivers and built-in software.

**Open source.**
Inasfar as the elements to store in the depot are already available in publicly accessible open source depots, it suffices to merely reference these sources with up-to-date building instructions for any device that is sold in Europe.  This requires that the open source depots are managed independently from the manufacturer.

**Locking.**
The contents of these depots may be made freely accessible to buyers of hardware at any time.  While a product continues to be produced and supported with security updates, any part of these depots can however be locked by the manufacturer.  When these conditions are broken, the depots are opened due to an automated EU procedure founded on (a) inability to supply new sales addresses, (b) failure to resolve security issues within 1 month, or (c) failure to repair failing functionality within 3 months.

**Customer protection.**
The main purpose of this depot is customer protection.  Hardware sold to Europeans must not stop to be useful on account of software issues for 100 years after purchase, and that requires long-term availability of software and manual.  Open source guarantees that users have an option to ask third parties to fix problems.  Europe arranges forums where users of a particular device can collect and meet with developers who might resolve driver problems.  During the locked phase, fixes by the manufacturer must be available free of charge.  During the open source phase, the formus support bidding on fixes and extensions and joint further development of the open source software and manual.

**Manufacturer benefits.**
Manufacturers can make an economically balanced end-of-life decision and could be among the first developers bidding to fix problems.  Unlocking may trigger a second round of hardware sales, and no support is required if the open source status is made explicit before the sale commences.  Until unlocking, the depot can be used as a locked software development and public binary release platform by manufacturers.  Software download facilities must be provided to upgrade the device.  These point to a development branch, which the customer can choose to change.

**Technical details.**
The depot runs on European GitLab software and provides an index for a customer, product and version.  Customers may be pointed at a start page for a manufacturer or a product, and browse for the downloads in a desired version or development versions from a given development branch.  Access control on development branches allows control over downloads, either before or after unlocking.  Produced hardware mentions version information that can be traced back into the platform.  The device should use the same [infrastructure to receive updates](https://raw.githubusercontent.com/fwupd/fwupd/master/docs/architecture-plan.svg), either via built-in software or via [external software](https://github.com/fwupd/fwupd) and a generally available or easily made connector to insert updates.  The download location and the version or branch and must support reconfiguration to allow customers to benefit from future branching of development.

**Independent software.**
Software may be sold to run on separately distributed hardware.  This ranges from applets on mobile devices to desktop applications and operating systems.  Such software falls under the same customer protection as software sold along with hardware.

**Dependent software.**
Software that falls under this customer protection may have dependencies on separately distributed software.  Example dependencies are third-party drivers, libraries or kernels.  Compatibility with new releases of such dependencies falls under the functional repairs protected herein, except when the new release involves a security fix, in which case compatibility with such dependencies falls under the security reairs protected herein.  Indirect dependencies are treated with the same delays as direct dependencies.

**Signing keys.**
Software may need to be signed to facilitate downloads.  This benefits security, but can also lock users out of their devices.  To this end, the platform must hold such signing keys and make them accessible to those who manage a branch; other branches receive their own signing keys; device owners must be able to change the signing key for their device when they change to another development branch.  Keys used for decryption of software are treated like signing keys.

**Automatic testing.**
It would be possible to automatically build and package software and firmware, including branch-specific signing, using a mechanism such as Continuus Integration.  This can be used to validate that the entire chain is available.  Binary files in the source repository can be recognised and analysed for dependencies, and may be rejected as repository content.  Regular updates to such binaries in the source tree may be flagged as a circumvention risk.  This aids manufacturers in compliancy when they use the platform, which is more comfortable than consumers rights being exercised to return end-of-life devices on the basis of software issues such as lacking security updates or incompatible firmware and lacking problem fixes.

